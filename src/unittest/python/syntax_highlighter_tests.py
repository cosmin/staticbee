import unittest
import xml.etree.ElementTree as ET

from staticbee.syntax_highlighter import SyntaxHighlighter


class SyntaxHighlighterTest(unittest.TestCase):

    def test_highlight(self):
        input_xml = '<root xmlns:sb="https://cosmin.hume.ro/project/staticbee"><sb:code lang="python"><![CDATA[import unittest]]></sb:code></root>'
        expected_output = '<root><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">unittest</span>\n</pre></div></root>'
        xml = ET.XML(input_xml)

        sh = SyntaxHighlighter()
        sh.highlight(xml)

        self.assertEqual(
            expected_output,
            ET.tostring(
                xml,
                encoding='utf8',
                method='html').decode('utf8'))

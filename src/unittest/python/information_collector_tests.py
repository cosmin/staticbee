import os
import unittest

from datetime import date
from staticbee.information_collector import InformationCollector


class InformationCollectorTest(unittest.TestCase):

    def test_collect_information(self):
        collector = InformationCollector()
        posts_res_dir = res_dir()
        content_info = collector.collect_information(posts_res_dir)
        posts = content_info.posts
        self.assertEqual(
            2,
            len(posts),
            f"Wrong number of posts in {posts_res_dir}")
        self.assertEqual(date(2020, 5, 31), posts[0].date)
        self.assertEqual("/post/2020/05/woodpecker", posts[0].url)
        self.assertEqual("Woodpecker", posts[0].title)
        self.assertSequenceEqual(["bird", "species"], [
                                 tag.label for tag in posts[0].tags])
        self.assertSequenceEqual(["nature", "featured"], [
                                 cat.label for cat in posts[0].categories])


def res_dir():
    current_dir = os.path.dirname(os.path.realpath(__file__))
    return os.path.realpath(
        os.path.join(
            current_dir,
            '..',
            'resources'))

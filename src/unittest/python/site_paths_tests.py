import unittest

from staticbee.site_paths import SitePaths


class SitePathsTest(unittest.TestCase):

    def test_layout_dir(self):
        paths = SitePaths("/tmp")
        layout_path = paths.layout_dir()
        layout_string = str(layout_path)
        self.assertEqual("/tmp/_layout", layout_string)

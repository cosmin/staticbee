import logging
import unittest
import xml.etree.ElementTree as ET

from staticbee.loop_unwinder import LoopUnwinder
from staticbee.model.content_info import ContentInfo
from staticbee.model.post import Post


class LoopUnwinderTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logging.basicConfig(level=logging.DEBUG)

    def test_unwind_loop(self):
        input_xml = '<root xmlns:sb="https://cosmin.hume.ro/project/staticbee"><sb:each of="posts" as="post"><div><a class="none" sb:text="post.title" sb:href="post.url"></a></div><br /></sb:each></root>'
        expected_output = '<root><div><a class="none" href="/post/one">Post One</a></div><br><div><a class="none" href="/post/two">Post Two</a></div><br></root>'
        xml = ET.XML(input_xml)

        content_info = ContentInfo()
        content_info.posts = [
            Post(title="Post One", url="/post/one"),
            Post(title="Post Two", url="/post/two")
        ]

        lu = LoopUnwinder()
        lu.unwind_loops(xml, content_info, "abcd")

        self.assertEqual(
            expected_output,
            ET.tostring(
                xml,
                encoding='utf8',
                method='html').decode('utf8'))

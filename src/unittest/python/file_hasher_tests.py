import os
import unittest

from staticbee.file_hasher import FileHasher


class FileHasherTest(unittest.TestCase):

    def test_hash(self):
        file_hash = FileHasher().hash(res('page.xml'))
        self.assertEqual("e36e8b13da2e5a7146a6dea35494e575e3825858", file_hash)


def res(filename):
    current_dir = os.path.dirname(os.path.realpath(__file__))
    return os.path.realpath(
        os.path.join(
            current_dir,
            '..',
            'resources',
            filename))

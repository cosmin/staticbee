import os
import unittest
import xml.etree.ElementTree as ET

from pathlib import Path
from staticbee.file_type_detector import FileTypeDetector


class FileTypeDetectorTest(unittest.TestCase):

    def setUp(self):
        self.detector = FileTypeDetector()

    def test_is_file_post_yes(self):
        result = self.detector.is_file_post(
            Path(
                res_dir(),
                'post',
                '2020',
                '05',
                'blackbird',
                'index.xml'))
        self.assertTrue(result)

    def test_is_file_post_no(self):
        result = self.detector.is_file_post(
            Path(res_dir(), 'post', 'index.xml'))
        self.assertFalse(result)

    def test_is_xml_post_yes(self):
        input_xml = '<sb:post xmlns:sb="https://cosmin.hume.ro/project/staticbee"></sb:post>'
        xml = ET.XML(input_xml)
        result = self.detector.is_xml_post(xml)
        self.assertTrue(result)

    def test_is_xml_post_no(self):
        input_xml = '<html></html>'
        xml = ET.XML(input_xml)
        result = self.detector.is_xml_post(xml)
        self.assertFalse(result)


def res_dir():
    current_dir = os.path.dirname(os.path.realpath(__file__))
    return os.path.realpath(
        os.path.join(
            current_dir,
            '..',
            'resources'))

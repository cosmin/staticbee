import os
import tempfile
import unittest

from pathlib import Path

from staticbee.layout_applier import LayoutApplier
from staticbee.model.content_info import ContentInfo


class LayoutApplierTest(unittest.TestCase):

    def test_use_handler_once(self):
        layout_handler = LayoutApplier(res("layout.xhtml"))
        self._test_apply_layout(layout_handler)

    def test_handler_can_be_reused(self):
        layout_handler = LayoutApplier(res("layout.xhtml"))
        self._test_apply_layout(layout_handler)
        self._test_apply_layout(layout_handler)

    def _test_apply_layout(self, layout_handler):
        with tempfile.NamedTemporaryFile(suffix='.html') as tmp:
            result = layout_handler.handle(
                res("page.xml"),
                Path(tmp.name),
                ContentInfo())
            self.assertIsNone(result)


def res(filename):
    current_dir = os.path.dirname(os.path.realpath(__file__))
    return os.path.realpath(
        os.path.join(
            current_dir,
            '..',
            'resources',
            filename))

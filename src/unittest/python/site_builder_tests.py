import os
import tempfile
import unittest

from staticbee.file_handler import FileHandler
from staticbee.file_handler_factory import FileHandlerFactory
from staticbee.information_collector import InformationCollector
from staticbee.site_builder import SiteBuilder

from mockito import mock, when, unstub
from pathlib import Path


class SiteBuilderTest(unittest.TestCase):

    def test_should_return_ok(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            when(os).walk(...).thenReturn([
                (tmpdirname, ['style'], ['index.html']),
                (f"{tmpdirname}/style", [], ['main.css'])
            ])

            mock_file_handler = mock(FileHandler)
            when(mock_file_handler).handle(...).thenReturn(None)

            mock_file_handler_factory = mock(FileHandlerFactory)
            when(mock_file_handler_factory).handler(...).thenReturn(
                mock_file_handler)

            mock_information_collector = mock(InformationCollector)
            when(mock_information_collector).collect_information().thenReturn([])

            SiteBuilder(
                tmpdirname,
                mock_information_collector,
                mock_file_handler_factory).build()

            self.assertTrue(Path(tmpdirname, "_site").exists())

            # restore os.walk
            unstub()

import unittest

from pathlib import Path
from staticbee.file_copier import FileCopier
from staticbee.file_handler_factory import FileHandlerFactory
from staticbee.file_type_detector import FileTypeDetector
from staticbee.layout_applier import LayoutApplier

from mockito import when, unstub


class FileHandlerFactoryTest(unittest.TestCase):

    def test_should_return_file_copier(self):
        input_xml = Path("example.xml")
        detector = FileTypeDetector()
        when(detector).is_file_transformable(input_xml).thenReturn(False)

        result = FileHandlerFactory().handler(Path("example.png"))

        self.assertIsInstance(result, FileCopier)

    def test_should_return_default_layout_applier(self):
        input_xml = Path("example.xml")
        detector = FileTypeDetector()
        when(detector).is_file_transformable(input_xml).thenReturn(True)
        when(detector).is_file_post(input_xml).thenReturn(False)

        result = FileHandlerFactory(detector).handler(input_xml)

        self.assertIsInstance(result, LayoutApplier)
        unstub()

    def test_should_return_posts_layout_applier(self):
        input_xml = Path("example.xml")
        detector = FileTypeDetector()
        when(detector).is_file_transformable(input_xml).thenReturn(True)
        when(detector).is_file_post(input_xml).thenReturn(True)

        result = FileHandlerFactory(detector).handler(input_xml)

        self.assertIsInstance(result, LayoutApplier)
        unstub()

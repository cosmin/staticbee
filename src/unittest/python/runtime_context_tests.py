from staticbee import runtime_context
import unittest


class RuntimeContextTest(unittest.TestCase):

    def test_init_site_paths(self):
        runtime_context.init_site_paths("/tmp")
        layout_path = runtime_context.site['paths'].layout_dir()
        layout_string = str(layout_path)
        self.assertEqual("/tmp/_layout", layout_string)

import xml.etree.ElementTree as ET

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter


class SyntaxHighlighter:

    __SB_SCHEMA_LOCATION = 'https://cosmin.hume.ro/project/staticbee'
    __SB_CODE_TAG = 'code'
    __SB_CODE_LANG_ATTR = 'lang'

    def __init__(self):
        self.ns = {'sb': SyntaxHighlighter.__SB_SCHEMA_LOCATION}

    def highlight(self, xml_root):
        layout_parent_map = {c: p for p in xml_root.iter() for c in p}
        for code_element in xml_root.findall(
                f".//sb:{SyntaxHighlighter.__SB_CODE_TAG}", self.ns):
            code_lang = code_element.attrib[SyntaxHighlighter.__SB_CODE_LANG_ATTR]
            code_content = code_element.text.strip()
            highlighted = highlight(
                code_content,
                get_lexer_by_name(code_lang),
                HtmlFormatter())

            highlighted_xml = ET.XML(highlighted)

            parent_of_code = layout_parent_map[code_element]
            index_of_code = list(parent_of_code).index(code_element)
            parent_of_code.insert(index_of_code, highlighted_xml)
            parent_of_code.remove(code_element)

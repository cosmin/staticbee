from pathlib import Path
from .model.content_info import ContentInfo


class FileHandler:

    def handle(self, src: Path, dest: Path, content_info: ContentInfo):
        """Handle a file"""
        pass

import hashlib


class FileHasher:

    __BLOCKSIZE = 64 * 1024

    def hash(self, file_path):
        # Yes, SHA-1 is weak, but it's fast and here
        # we need this only to tell files apart;
        # there are no security considerations.
        # If SHA-1 is good enough for Git,
        # then it's good enough for StaticBee too.
        hasher = hashlib.sha1()

        with open(file_path, 'rb') as input_file:
            buffer = input_file.read(FileHasher.__BLOCKSIZE)
            while len(buffer) > 0:
                hasher.update(buffer)
                buffer = input_file.read(FileHasher.__BLOCKSIZE)
        return hasher.hexdigest()

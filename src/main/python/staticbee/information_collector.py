import logging
import os
import xml.etree.ElementTree as ET

from datetime import date
from pathlib import Path
from . import runtime_context
from .model.category import Category
from .model.post import Post
from .model.tag import Tag
from .model.content_info import ContentInfo
from .file_hasher import FileHasher
from .file_type_detector import FileTypeDetector


logger = logging.getLogger(__name__)


class InformationCollector:

    __SB_SCHEMA_LOCATION = 'https://cosmin.hume.ro/project/staticbee'

    def __init__(self,
                 file_type_detector: FileTypeDetector = FileTypeDetector(),
                 file_hasher: FileHasher = FileHasher()):
        self.ns = {'sb': InformationCollector.__SB_SCHEMA_LOCATION}
        self.site_config = runtime_context.site
        self.base_dir = ""
        self.file_type_detector = file_type_detector
        self.file_hasher = file_hasher

    def collect_information(self, posts_dir=None):
        content_info = ContentInfo()
        self.base_dir = posts_dir or self.site_config['paths'].base_dir()
        for root, _, files in os.walk(self.base_dir):
            for file in files:
                if self.__is_emacs_lock_file(file):
                    continue
                src = Path(root, file)
                self.__read_post_from_file(src, content_info.posts)
        content_info.posts.sort(key=lambda post: post.date, reverse=True)

        logger.debug("Collected %s posts", len(content_info.posts))
        return content_info

    def __is_emacs_lock_file(self, src):
        return src.startswith(".#")

    def __read_post_from_file(self, src: Path, posts):
        if src.suffix == '.xml':
            xml = ET.parse(src)
            xml_root = xml.getroot()
            if self.file_type_detector.is_xml_post(xml_root):
                post = Post()
                self.__read_hash(src, post)
                self.__read_url(src, post)
                self.__read_title(xml_root, post)
                self.__read_date(xml_root, post)
                self.__read_tags(xml_root, post)
                self.__read_categories(xml_root, post)
                posts.append(post)

    def __read_hash(self, src: Path, post):
        post.file_hash = self.file_hasher.hash(src)
        logger.debug("File %s has hash %s", src, post.file_hash)

    def __read_url(self, src: Path, post):
        if src.stem == 'index':
            path_for_url = src.parent
        else:
            path_for_url = src
        rel_dir = path_for_url.relative_to(self.base_dir)
        post.url = '/' + str(rel_dir)

    def __read_title(self, xml_root, post):
        title_node = xml_root.find(f".//sb:title", self.ns)
        post.title = title_node.text

    def __read_date(self, xml_root, post):
        date_node = xml_root.find(f".//sb:date", self.ns)
        post.date = date.fromisoformat(date_node.text)

    def __read_tags(self, xml_root, post):
        for tag_node in xml_root.findall(f".//sb:tag", self.ns):
            tag = Tag(
                label=tag_node.text,
                url=self.site_config['paths'].tag_url_prefix() + tag_node.text)
            post.tags.append(tag)

    def __read_categories(self, xml_root, post):
        for category_node in xml_root.findall(f".//sb:category", self.ns):
            category = Category(
                label=category_node.text,
                url=self.site_config['paths'].category_url_prefix() +
                category_node.text)
            post.categories.append(category)

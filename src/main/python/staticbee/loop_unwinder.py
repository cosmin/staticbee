import copy
import logging
import re

from .model.content_info import ContentInfo


logger = logging.getLogger(__name__)


class LoopUnwinder:

    __SB_SCHEMA_LOCATION = 'https://cosmin.hume.ro/project/staticbee'
    __SB_EACH_TAG = 'each'
    __SB_FOR_COLLECTION_ATTR = 'of'
    __SB_FOR_VAR_ATTR = 'as'

    __OBJECT_DOT_FIELD_PATTERN = re.compile("([a-zA-Z0-9]+)\\.([a-zA-Z0-9]+)")

    def __init__(self):
        self.ns = {'sb': LoopUnwinder.__SB_SCHEMA_LOCATION}

    def unwind_loops(self, xml_root, content_info: ContentInfo, file_hash):
        layout_parent_map = {c: p for p in xml_root.iter() for c in p}
        for for_element in xml_root.findall(
                f".//sb:{LoopUnwinder.__SB_EACH_TAG}", self.ns):
            collection_name = for_element.attrib[LoopUnwinder.__SB_FOR_COLLECTION_ATTR]
            var_name = for_element.attrib[LoopUnwinder.__SB_FOR_VAR_ATTR]

            collection = self.__get_collection(
                collection_name, content_info, file_hash)
            logger.debug(
                "Looping over %s %s",
                len(collection),
                collection_name)

            parent_of_for = layout_parent_map[for_element]
            for item in collection:
                for for_child in for_element:
                    iterated_child = copy.deepcopy(for_child)
                    self.__apply_to_all(item, iterated_child, var_name)
                    parent_of_for.append(iterated_child)
            parent_of_for.remove(for_element)

    def __get_collection(
            self,
            collection_name,
            content_info: ContentInfo,
            file_hash):
        if collection_name == 'posts':
            collection = content_info.posts
        elif collection_name == 'post.tags':
            logger.debug("Reading tags for file with hash %s", file_hash)
            for post in content_info.posts:
                if post.file_hash == file_hash:
                    logger.debug("Reading tags for post %s", post.title)
                    collection = post.tags
        elif collection_name == 'post.categories':
            logger.debug("Reading categories for file with hash %s", file_hash)
            for post in content_info.posts:
                if post.file_hash == file_hash:
                    logger.debug("Reading categories for post %s", post.title)
                    collection = post.categories
        else:
            collection = []
        return collection or []

    def __apply_to_all(self, collection_item, element, var_name):
        for e in element.iter():
            self.__apply(collection_item, e, var_name)

    def __apply(self, collection_item, element, var_name):
        handled_attributes = []
        attributes_to_set = []
        for attribute in element.attrib:
            if attribute == self.__sb_attribute("text"):
                self.__handle_sb_text(
                    collection_item, element, attribute, var_name)
                handled_attributes.append(attribute)
            elif attribute == self.__sb_attribute("href"):
                href_value = self.__get_sb_href(
                    collection_item, element, attribute, var_name)
                if href_value:
                    attributes_to_set.append(("href", href_value))
                handled_attributes.append(attribute)
        for (key, value) in attributes_to_set:
            logger.debug("Setting attribute %s = %s", key, value)
            element.set(key, value)
        for handled_attribute in handled_attributes:
            element.attrib.pop(handled_attribute)

    def __sb_attribute(self, attribute_name):
        return "{" + LoopUnwinder.__SB_SCHEMA_LOCATION + "}" + attribute_name

    def __handle_sb_text(self, collection_item, element, attribute, var_name):
        match = LoopUnwinder.__OBJECT_DOT_FIELD_PATTERN.match(
            element.attrib[attribute])
        if match:
            object_name = match.group(1)
            field_name = match.group(2)
            if object_name == var_name:
                try:
                    attribute_value = getattr(collection_item, field_name)
                except AttributeError:
                    logger.exception("Failed to get attribute")
                else:
                    logger.debug(
                        "Setting text attribute for %s = [%s]",
                        element.tag,
                        attribute_value)
                    element.text = attribute_value

    def __get_sb_href(self, collection_item, element, attribute, var_name):
        match = LoopUnwinder.__OBJECT_DOT_FIELD_PATTERN.match(
            element.attrib[attribute])
        if match:
            object_name = match.group(1)
            field_name = match.group(2)
            if object_name == var_name:
                try:
                    attribute_value = getattr(collection_item, field_name)
                except AttributeError:
                    logger.exception("Failed to get attribute")
                else:
                    logger.debug(
                        "Reading href attribute for %s = [%s]",
                        element.tag,
                        attribute_value)
                    return attribute_value

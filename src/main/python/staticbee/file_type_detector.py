import xml.etree.ElementTree as ET

from pathlib import Path


class FileTypeDetector:

    __SB_SCHEMA_LOCATION = 'https://cosmin.hume.ro/project/staticbee'

    def is_file_transformable(self, file: Path):
        return self.__is_file_with_content(
                file,
                lambda f: self.is_xml_post(f) or self.is_xml_page(f)
        )

    def is_file_post(self, file: Path):
        return self.__is_file_with_content(file, self.is_xml_post)

    def __is_file_with_content(self, file: Path, fn):
        if file.suffix == '.xml':
            xml = ET.parse(file)
            xml_root = xml.getroot()
            return fn(xml_root)
        else:
            return False

    def is_xml_page(self, element: ET.Element):
        return element.tag == self.__sb_tag("page")

    def is_xml_post(self, element: ET.Element):
        return element.tag == self.__sb_tag("post")

    def __sb_tag(self, tag):
        return "{" + FileTypeDetector.__SB_SCHEMA_LOCATION + "}" + tag

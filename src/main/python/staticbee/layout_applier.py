import logging
import xml.etree.ElementTree as ET

from pathlib import Path
from .file_hasher import FileHasher
from .loop_unwinder import LoopUnwinder
from .syntax_highlighter import SyntaxHighlighter
from .model.content_info import ContentInfo


logger = logging.getLogger(__name__)


class LayoutApplier:

    __HTML_NAMESPACE = 'http://www.w3.org/1999/xhtml'
    __SB_SCHEMA_LOCATION = 'https://cosmin.hume.ro/project/staticbee'

    def __init__(self, layout_xml, file_hasher: FileHasher = FileHasher()):
        self.syntax_highlighter = SyntaxHighlighter()
        self.loop_unwinder = LoopUnwinder()
        self.file_hasher = file_hasher

        self.ns = {'sb': LayoutApplier.__SB_SCHEMA_LOCATION}
        ET.register_namespace('', LayoutApplier.__HTML_NAMESPACE)
        self.layout_xml = layout_xml
        self.layout = None

    def handle(self, src: Path, dest: Path, content_info: ContentInfo):
        self.__read_layout()
        page = ET.parse(src)
        file_hash = self.file_hasher.hash(src)
        self.page_root = page.getroot()
        logger.debug("Pre-process file %s with hash %s", src, file_hash)
        self.__preprocess(content_info, file_hash)
        self.__replace_tags(dest.with_suffix('.html'))

    def __read_layout(self):
        self.layout = ET.parse(self.layout_xml)
        self.layout_root = self.layout.getroot()
        self.layout_parent_map = {c: p for p in self.layout.iter() for c in p}

    def __preprocess(self, content_info: ContentInfo, file_hash):
        self.loop_unwinder.unwind_loops(
            self.layout_root, content_info, file_hash)
        self.loop_unwinder.unwind_loops(
            self.page_root, content_info, file_hash)
        self.syntax_highlighter.highlight(self.page_root)

    def __replace_tags(self, dest):
        self.__replace('title')
        self.__replace('date')
        self.__replace('content')
        with open(dest, 'w') as out:
            out.write('<!DOCTYPE html>' + '\n')
            self.layout.write(out, encoding='unicode', method='html')
        #print(ET.tostring(self.layout_root, encoding='utf8').decode('utf8'))

    def __replace(self, tag):
        logger.debug("Replacing %s", tag)
        for layout_content in self.layout_root.findall(
                f".//sb:{tag}", self.ns):
            page_content = self.page_root.find(f".//sb:{tag}", self.ns)
            logger.debug("Page content is %s", page_content.tag)
            inner_xml = page_content.text
            if inner_xml and inner_xml.strip():
                logger.debug("Set text [%s]", inner_xml)
                self.layout_parent_map[layout_content].text = inner_xml
            else:
                logger.debug("Appending %s children", len(page_content))
                for page_child in page_content:
                    logger.debug("Append %s", page_child.tag)
                    self.layout_parent_map[layout_content].append(page_child)
            self.layout_parent_map[layout_content].remove(layout_content)

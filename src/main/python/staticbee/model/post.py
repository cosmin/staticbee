from datetime import date


class Post:

    def __init__(
            self,
            file_hash="",
            date=date.min,
            url="",
            title="",
            tags=None,
            categories=None):
        self.file_hash = file_hash
        self.date = date
        self.url = url
        self.title = title
        self.tags = tags or []
        self.categories = categories or []

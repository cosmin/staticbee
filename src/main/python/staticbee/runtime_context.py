from .site_paths import SitePaths

site = {
    'paths': SitePaths()
}


def init_site_paths(dirname=None):
    site['paths'] = SitePaths(dirname)
    return site

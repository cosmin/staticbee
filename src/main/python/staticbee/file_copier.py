from .file_handler import FileHandler
from .model.content_info import ContentInfo
from pathlib import Path
from shutil import copyfile


class FileCopier(FileHandler):

    def handle(self, src: Path, dest: Path, content_info: ContentInfo):
        copyfile(src, dest)

from pathlib import Path

from .file_copier import FileCopier
from .file_handler import FileHandler
from .file_type_detector import FileTypeDetector
from .layout_applier import LayoutApplier
from . import runtime_context


class FileHandlerFactory:

    def __init__(self,
                 file_type_detector: FileTypeDetector = FileTypeDetector()):
        self.file_type_detector = file_type_detector
        self.file_copier = FileCopier()
        self.site_config = runtime_context.site
        self.layout_appliers = {}

    def handler(self, file: Path) -> FileHandler:
        if self.file_type_detector.is_file_transformable(file):
            return self.__get_layout_applier(file)
        else:
            return self.file_copier

    def __get_layout_applier(self, file: Path):
        if self.file_type_detector.is_file_post(file):
            layout = 'post'
        else:
            layout = 'default'
        if layout not in self.layout_appliers:
            self.layout_appliers[layout] = LayoutApplier(
                Path(self.site_config['paths'].layout_dir(), layout + '.xhtml'))
        return self.layout_appliers[layout]

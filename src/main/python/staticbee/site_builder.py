import logging
import os

from . import runtime_context

from pathlib import Path
from .file_handler_factory import FileHandlerFactory
from .information_collector import InformationCollector


logger = logging.getLogger(__name__)


class SiteBuilder:

    def __init__(
            self,
            dirname=None,
            information_collector: InformationCollector = InformationCollector(),
            file_handler_factory: FileHandlerFactory = FileHandlerFactory()):
        site_config = runtime_context.init_site_paths(dirname)
        self.site_paths = site_config['paths']
        self.information_collector = information_collector
        self.file_handler_factory = file_handler_factory

    def build(self):
        content_info = self._collect_information()
        self._process_files(content_info)

    def _collect_information(self):
        return self.information_collector.collect_information()

    def _process_files(self, content_info):
        Path(
            self.site_paths.output_dir()).mkdir(
            parents=True,
            exist_ok=True)

        exclude = set([self.site_paths.layout_dir_name(),
                       self.site_paths.output_dir_name()])
        for root, dirs, files in os.walk(self.site_paths.base_dir()):
            # modify the directory names list in-place
            dirs[:] = [d for d in dirs if d not in exclude]
            if files:
                rel_dir = os.path.relpath(root, self.site_paths.base_dir())
                dest_dir = Path(self.site_paths.output_dir(), rel_dir)
                dest_dir.mkdir(parents=True, exist_ok=True)
                for file in files:
                    if self.__is_emacs_lock_file(file):
                        continue
                    src = Path(root, file)
                    dest = Path(dest_dir, file)
                    logger.debug("Handling file %s", src)
                    self.file_handler_factory.handler(
                        src).handle(src, dest, content_info)

    def __is_emacs_lock_file(self, src):
        return src.startswith(".#")

import os

from pathlib import Path


class SitePaths:

    __LAYOUT_DIR = "_layout"
    __OUTPUT_DIR = "_site"
    __TAG_URL_PREFIX = "/post/tag/"
    __CATEGORY_URL_PREFIX = "/post/category/"

    def __init__(self, dirname=None):
        self.__workdir = dirname or os.getcwd()

    def base_dir(self):
        return self.__workdir

    def layout_dir(self):
        return Path(self.__workdir, SitePaths.__LAYOUT_DIR)

    def layout_dir_name(self):
        return SitePaths.__LAYOUT_DIR

    def output_dir(self):
        return Path(self.__workdir, SitePaths.__OUTPUT_DIR)

    def output_dir_name(self):
        return SitePaths.__OUTPUT_DIR

    def tag_url_prefix(self):
        return SitePaths.__TAG_URL_PREFIX

    def category_url_prefix(self):
        return SitePaths.__CATEGORY_URL_PREFIX

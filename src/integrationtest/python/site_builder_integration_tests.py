import logging
import os
import tempfile
import unittest

from distutils.dir_util import copy_tree
from pathlib import Path

from staticbee.site_builder import SiteBuilder


class SiteBuilderIntegrationTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logging.basicConfig(level=logging.DEBUG)

    def test_build_site(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            current_dir = os.path.dirname(os.path.realpath(__file__))
            res_site_dir = os.path.realpath(
                os.path.join(
                    current_dir,
                    '..',
                    'resources',
                    'test_site'))
            copy_tree(res_site_dir, tmpdirname)

            SiteBuilder(tmpdirname).build()

            self.assertTrue(Path(tmpdirname, "_site").exists())
            with open(Path(tmpdirname, "_site", "post", "index.html"), "r") as blog_index:
                index_content = blog_index.read()
                print(index_content)
            with open(Path(tmpdirname, "_site", "post", "2020", "05", "blackbird", "index.html"), "r") as post_index:
                index_content = post_index.read()
                print(index_content)

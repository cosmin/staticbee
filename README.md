# Static Bee
## Static Site Generator

Static Bee is a static site generator with no configuration, based on XML.

The structure of your site is a mirror of the structure of your sources.

The main idea is: `layout + page = result`.

For example, consider the following XHTML file as a layout...

```xml
<!DOCTYPE html>
<html xmlns:sb="https://cosmin.hume.ro/project/staticbee">
  <head>
    <title>
      <sb:title />
    </title>
  </head>
  <body>
    <sb:content />
  </body>
</html>
```

... and the following XML file as a page.

```xml
<?xml version='1.0' encoding='utf8'?>
<sb:page xmlns:sb="https://cosmin.hume.ro/project/staticbee">
  <sb:title>Example page</sb:title>
  <sb:content>
    <h1>Hello, world!</h1>
    <p>This is some content</p>
  </sb:content>
</sb:page>
```

Static Bee combines these two together and produces a HTML page for your site:

```xml
<!DOCTYPE html>
<html>
  <head>
    <title>Example page</title>
  </head>
  <body>
    <h1>Hello, world!</h1>
    <p>This is some content</p>
  </body>
</html>
```

## Development

To build Static Bee, you need Python 3 and [PyBuilder](https://pybuilder.io/).

```bash
python3 -m venv venv
source venv/bin/activate
pip install pybuilder mockito pygments
```

In the project directory, run:

```bash
pyb clean publish
```

To install:

```bash
cd target/dist/staticbee-1.0.dev0/
python3 setup.py install --user
```

Then edit `~/.local/bin/staticbee` and fix the shebang:

```bash
sed -i 's/#!python/#!\/usr\/bin\/env python3/' ~/.local/bin/staticbee
```

Or do all these steps at once by running `make` in the project directory.

### Dependencies

Dependencies are managed by PyBuilder.

For development we use:
* [Mockito](https://mockito-python.readthedocs.io/en/latest/)

At runtime we need:
* [Pygments](https://pygments.org/), for highlighting syntax in the content of your site
